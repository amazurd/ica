import {Component, OnInit} from '@angular/core';
import {SharedService} from "../../utility/shared-service/shared.service";
import {Web3Service} from "../../utility/shared-service/web3.service";
import {slideUp} from "../animation";

@Component({
  selector: 'app-referral-link',
  templateUrl: './referral-link.component.html',
  styleUrls: ['./referral-link.component.css'],
  animations: [slideUp]
})
export class ReferralLinkComponent implements OnInit {

  referralLink: string = "";

  constructor(private sharedService: SharedService, private web3Service: Web3Service) {

  }

  ngOnInit() {

    var user = this.sharedService.getUser();
    // console.log(user);
    this.referralLink = "tokensale.icareum.io/?ref="+user.referralCode;
  }

  // checkAccountBalance(account) {
  //   this.web3Service.getTokenBalance(account).subscribe(result=> {
  //     this.balance = result;
  //     this.loaderProof = false;
  //   }, error=> {
  //     this.balance = error;
  //     //// console.log(error);
  //   });
  // }

  // tokenBalanceRefresh() {
  //   this.loaderProof = true;
  //   this.sharedService.trackMixPanelEvent("Refresh Proof Balance");
  //   this.balance = "0";
  //   this.checkAccountBalance(this.sharedService.getWalletAddress.address);
  // }

}
