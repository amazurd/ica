import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderSidebarComponent } from './header-sidebar/header-sidebar.component';
import {AdminRoutingModule} from "./admin-routing.module";
import { DashboardComponent } from './dashboard/dashboard.component';
import {RouterModule} from "@angular/router";
import { TransactionHistoryComponent } from './transaction-history/transaction-history.component';
import { SendComponent } from './send/send.component';
import { BalancesComponent } from './balances/balances.component';
import { ReferralLinkComponent } from './referral-link/referral-link.component';
import { AddressInfoComponent } from './address-info/address-info.component';
import { BuyICRMComponent } from './buy-icrm/buy-icrm.component';
import { PresaleComponent } from './presale/presale.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import {UtilityModule} from "../utility/utility.module";
import {
  DataTableModule,
  DropdownModule,
  InputSwitchModule,
  MessagesModule,
  SharedModule,
  TooltipModule
} from "primeng/primeng";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {NgxQRCodeModule} from "ngx-qrcode2";
import { SettingsComponent } from './settings/settings.component';
import { EtheriumComponent } from './header-sidebar/etherium/etherium.component';
import { LoadWalletComponent } from './header-sidebar/load-wallet/load-wallet.component';
import { BuyTokensComponent } from './header-sidebar/buy-tokens/buy-tokens.component';
import {MyAutofocusDirective} from "./focus.directive";
import { FooterComponent } from './footer/footer.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    DropdownModule,
    TooltipModule,
    DataTableModule,
    MessagesModule,
    SharedModule,
    InputSwitchModule,
    FormsModule,
    NgxQRCodeModule,
    ReactiveFormsModule,
    UtilityModule,
    AdminRoutingModule
  ],
  declarations: [
    HeaderSidebarComponent,
    DashboardComponent,
    TransactionHistoryComponent,
    SendComponent,
    BalancesComponent,
    ReferralLinkComponent,
    AddressInfoComponent,
    BuyICRMComponent,
    PresaleComponent,
    UserProfileComponent,
    SettingsComponent,
    EtheriumComponent,
    LoadWalletComponent,
    BuyTokensComponent,
    FooterComponent
  ],
  exports:[HeaderSidebarComponent]
})
export class AdminModule { }
