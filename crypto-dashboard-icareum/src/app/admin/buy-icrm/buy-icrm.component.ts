import {Component, OnInit, ViewContainerRef} from '@angular/core';
import {SharedService} from "../../utility/shared-service/shared.service";
import {Web3Service} from "../../utility/shared-service/web3.service";
import * as FileSaver from 'file-saver';
import {Wallet} from "../../utility/shared-model/wallet.model";
import {slideUp} from "../animation";
import {APIManager} from "../../utility/shared-service/apimanager.service";
import {API} from "../../utility/constants/api";
import {ToastsManager} from "ng2-toastr";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {style, transition, animate, trigger} from "@angular/animations";
import {BaseComponent} from "../../utility/base-component/base.component";

@Component({
  selector: 'buy-icrm-component',
  templateUrl: './buy-icrm.component.html',
  styleUrls: ['./buy-icrm.component.css'],
  animations: [
  trigger('dialog', [
      transition('void => *', [
        style({transform: 'scale3d(.8, .8, .8)'}),
        animate(300)
      ]),
      transition('* => void', [
        animate(200, style({transform: 'scale3d(.0, .0, .0)'}))
      ])
    ]),
  slideUp]
})



export class BuyICRMComponent implements OnInit {

  elementType: 'url' | 'canvas' | 'img' = 'url';
  address: string = "";

  bitcoinAddress: string = "";
  
  etherBalance: string = "0";
  tokenBalance: string = "0";
  wallet: Wallet;

  isShowModal: number = 1;
  walletAddressForm: FormGroup;
  calculatorForm: FormGroup;
  calculatorTokenValue : number = 0;

  BTCRate : number = 0.00003236;
  ETHRate : number = 0.00099502;
  DollarRate : number = 0.2;

  constructor(private sharedService: SharedService, 
    private web3Service: Web3Service,
    private apiManager: APIManager, 
    private tstManager: ToastsManager,
    private formBuilder: FormBuilder,
    public vcr: ViewContainerRef) {

      this.tstManager.setRootViewContainerRef(vcr);
  }

  ngOnInit() {
    this.address = API.COMPANY_ETHEREUM_ADDRESS;
    //this.etherAddress = API.COMPANY_ETHEREUM_ADDRESS //"0x" + this.sharedService.getWalletAddress.address;
    this.bitcoinAddress = API.COMPANY_BITCOIN_ADDRESS;
    this.checkAccountBalance(this.address);
   // this.checkProofAccountBalance(this.address);

    this.createWalletAddressForm();
    this.createCalculatorForm();
  }

  eth100() {
    return Number(this.ETHRate*100).toFixed(6);
  }
  btc100() {
    return Number(this.BTCRate*100).toFixed(6);
  }

  createWalletAddressForm() {
    this.walletAddressForm = this.formBuilder.group({
      walletAddress: new FormControl('',Validators.compose([Validators.minLength(10), Validators.maxLength(60)]))});//, Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(100), <any>Validators.pattern(CommonRegexp.EMAIL_ADDRESS_REGEXP)])),

  }

  createCalculatorForm() {
    this.calculatorForm = this.formBuilder.group({
      tokenAmount: new FormControl('')});//, Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(100), <any>Validators.pattern(CommonRegexp.EMAIL_ADDRESS_REGEXP)])),

  }

  checkAccountBalance(account) {
    this.web3Service.getEtherAccountBalance(account).subscribe(result=> {
      this.etherBalance = result;
    }, error=> {
      this.etherBalance = error;
      //// console.log(error);
    });
  }

  backupWallet() {
    this.wallet = this.sharedService.getWalletAddress;
    var date = new Date();
    var filename = 'UTC--' + date.toISOString().replace(/:/g, '-') + '--' +
      this.wallet.address.slice(2);
    var json = JSON.stringify(this.wallet);
    var blob = new Blob([json], {type: "text/plain;charset=utf-8"});
    FileSaver.saveAs(blob, filename);
  }

  showWalletAddressModal() {
 //   console.log("showwalletaddressmodal")
    this.createWalletAddressForm();
    this.isShowModal = 2;
  }

    showCalculatorModal() {
 //   console.log("showwalletaddressmodal")
    this.createCalculatorForm();
    this.isShowModal = 3;
  }

  closeForm(){
    this.isShowModal = 1;
  }

  sendConfirmationEmail(walletAddress) {
    this.isShowModal = 1;
   // // console.log(walletAddress)

            let params = {
      userId: this.sharedService.getUser().id,
      email: this.sharedService.getUser().email,
      btc: this.BTCRate,
      eth: this.ETHRate,
      wallet: walletAddress
    }
    this.apiManager.putAPI(API.SEND_CONFIRMATION_EMAIL, params).subscribe(response=> {

    //  // console.log(response);
      // this.tstManager.success("Confirmation send successfully")
    }, error=> {
    //  // console.log(error);
       //this.tstManager.error("Error occured")
    });
  }
  checkProofAccountBalance(account) {
    this.web3Service.getTokenBalance(account).subscribe(result=> {
      this.tokenBalance = result;
    }, error=> {
      this.tokenBalance = error;
      //// console.log(error);
    });
  }

  copyMessage(val: string){
    let selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = val;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);

    //this.tstManager.info("Copy to clipboard");
  }
}
