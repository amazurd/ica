export class RouteConstants {
  public static LOGIN = 'login';
  public static REGISTERATION = 'register';
  public static COMPLETE_ICO = 'complete-ico';
  public static DASHBOARD = 'dashboard';
  public static TRANSACTION_HISTORY = 'transaction-history';
  public static SEND = 'send';
  public static BALANCES = 'balances';
  public static REFERRAL_LINK = 'referral-link';
  public static ADDRESS_INFO = 'address-info';
  public static PRESALE = 'presale';
  public static BUY_ICRM = 'buy-icrm';
  public static SETTINGS = 'settings';
  public static USER_PROFILE = 'user-profile';
  public static CHANGE_PASSWORD = 'change-password';
  public static GET_ETHERIUM = 'ethereum';
  public static WALLET = 'wallet';
  public static BUY_TOKEN = 'buy-token';
  public static NOTFOUND = 'not-found';
}
