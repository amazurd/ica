'use strict';

var authController = require(BASE_PATH + '/app/auth/AuthController');
var router  = express.Router();

module.exports = (function() {
    router.post('/login', validatorClass.publicRouteValidate('login'), function (req, res, next) {
        authController.login(req, res);
    });

    router.post('/signup', validatorClass.publicRouteValidate('signup'), function (req, res, next) {
        authController.signup(req, res);
    });

    router.post('/verifyemailotp', validatorClass.publicRouteValidate('verifyemailotp'), function (req, res, next) {
        authController.verifyEmailOTP(req, res);
    });

    router.put('/resendemailotp', validatorClass.publicRouteValidate('resendemailotp'), function(req, res, next) {
        authController.resendEmailOtp(req, res);
    });

    // router.post('/forgotPassword', validatorClass.publicRouteValidate('forgotPassword'), function(req, res, next) {
    //     authController.forgotPassword(req, res);
    // });

    // router.post('/resetPassword', validatorClass.publicRouteValidate('resetPassword'), function(req, res, next) {
    //     authController.resetPassword(req, res);
    // });
    // router.post('/temporary_token_balance', validatorClass.publicRouteValidate('temporary_token_balance'), function(req, res, next) {
    //     authController.temporary_token_balance(req, res);
    // });
    // router.post('/send_confiramtion_email', validatorClass.publicRouteValidate('send_confiramtion_email'), function(req, res, next) {
    //     authController.send_confiramtion_email(req, res);
    // });
    // router.post('/referreraddress', validatorClass.publicRouteValidate('referreraddress'), function(req, res, next) {
    //     authController.referreraddress(req, res);
    // });

    return router;
})();